package agencia;

public class Auto {
	private String modelo;
	private String patente;

	public Auto(String modelo, String patente) {
		// TODO Auto-generated constructor stub
		this.modelo = modelo;
		this.patente = patente;
	}
	
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

}
