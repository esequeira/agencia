package agencia; 
import java.util.Collection;
import java.util.HashSet;


public class Agencia {

	protected Collection<Auto> autos;
	protected Collection<Chofer> choferes;
	protected Collection<Cliente> clientes;
	
	public Agencia() {
		this.setAutos(new HashSet<Auto>());
		this.setChofer(new HashSet<Chofer>());
		this.setCliente(new HashSet<Cliente>());
	}

	/* Coleccion de Autos*/
	public Collection<Auto> getAutos() {
		return this.autos;
	}

	public void setAutos(Collection<Auto> someAutos) {
		this.autos = someAutos;
	}

	public void addAutos(Auto aAuto) {
		this.getAutos().add(aAuto);
	}

	// Coleccion de Choferes
	
	public Collection<Chofer> getChoferes() {
		return this.choferes;
	}

	public void setChofer(Collection<Chofer> someChofer) {
		this.choferes = someChofer;
	}

	public void addChofer(Chofer aChofer) {
		this.getChoferes().add(aChofer);
	}
	
	// Coleccion de Clientes
	
	public Collection<Cliente> getClientes() {
		return this.clientes;
	}

	public void setCliente(Collection<Cliente> someCliente) {
		this.clientes = someCliente;
	}
	
	public void addCliente(Cliente aCliente) {
		this.getClientes().add(aCliente);
	}
}