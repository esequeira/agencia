package agencia;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Persistencia por alcance
		
		Agencia agencia = new Agencia();
		
		Auto auto1 = new Auto("Ford","GAD 354");
		Auto auto2 = new Auto("Chevrolet","TER 454");
		
		Chofer chofer1 = new Chofer("Juan", "Martin",  "30215462",  "20-154223-2",  "15-6542-1328",  "Aristobulo del Valle 1444", true, "1212A");
		
		Cliente cliente1 = new Cliente("Juan", "Martin",  "30215462",  "20-154223-2",  "15-6542-1328",  "Aristobulo del Valle 1444","20313030823","Ernesto Sequeira");

		agencia.addAutos(auto1);
		agencia.addAutos(auto2);
		agencia.addChofer(chofer1);
		agencia.addCliente(cliente1);
		
		System.out.println(agencia.getAutos());
		System.out.println(agencia.getChoferes());
		System.out.println(agencia.getClientes());
		
        }
}

