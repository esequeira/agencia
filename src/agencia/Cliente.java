package agencia;

public class Cliente extends Persona {
	String cuit;
	String razonSocial;

	
	public Cliente(String nombre, String apellido, String dni, String cuil, String celular, String direccion,String cuit, String razonSocial) {
		super(nombre, apellido, dni, cuil, celular, direccion);
		// TODO Auto-generated constructor stub
		this.cuit = cuit;
		this.razonSocial = razonSocial;
	}
	
	
    public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }
    
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

}