package agencia;

public class Chofer extends Persona {
	private Boolean disponibilidad;
	private String numeroLicencia;

	public Chofer(String nombre, String apellido, String dni, String cuil, String celular, String direccion, Boolean disponibilidad, String numeroLicencia) {

		super(nombre, apellido,  dni,  cuil,  celular,  direccion);
		this.disponibilidad = disponibilidad;
		this.numeroLicencia = numeroLicencia;
	}
	
	public Chofer() {
		super();
	}
	
    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public Boolean setDisponibilidad(Boolean disponibilidad) {
        return this.disponibilidad = disponibilidad;
    }
    
    public String getNumeroLicencia() {
        return numeroLicencia;
    }

    public void setNumeroLicencia(String numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }

}