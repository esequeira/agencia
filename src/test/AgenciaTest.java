package test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import agencia.Agencia;
import agencia.Auto;
import agencia.Chofer;
import agencia.Cliente;

class AgenciaTest{

	@Test
	final void testAgencia() {
		
		Agencia agencia = new Agencia();
		
		Auto auto1 = new Auto("Ford","GAD 354");
		Auto auto2 = new Auto("Chevrolet","TER 454");
		Chofer chofer1 = new Chofer("Juan", "Martin",  "30215462",  "20-154223-2",  "15-6542-1328",  "Aristobulo del Valle 1444", true, "1212A");
		Cliente cliente1 = new Cliente("Juan", "Martin",  "30215462",  "20-154223-2",  "15-6542-1328",  "Aristobulo del Valle 1444","20313030823","Ernesto Sequeira");

		agencia.addAutos(auto1);
		agencia.addAutos(auto2);
		agencia.addChofer(chofer1);
		agencia.addCliente(cliente1);

		Boolean resultChofer = chofer1.getDisponibilidad();
		String resultAuto = auto2.getPatente();
		String resultCliente = cliente1.getDni();
		
		assertEquals(true, resultChofer); // Un chofer
		assertEquals("TER 454", resultAuto); // Un auto
		assertEquals("30215462", resultCliente); // Un cliente
	}

}